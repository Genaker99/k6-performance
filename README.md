# K6-performance

## Installation

## Linux
## Debian/Ubuntu
```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 379CE192D401AB61
echo "deb https://dl.bintray.com/loadimpact/deb stable main" | sudo tee -a /etc/apt/sources.list
sudo apt-get update
sudo apt-get install k6
```

If you are behind a firewall or proxy
There have been reports of users being unable to download the key from Ubuntu's key-server using apt-key command due to firewalls or proxies blocking their requests. If you experience this issue, you may try this alternative approach instead:
```
wget -q -O - https://bintray.com/user/downloadSubjectPublicKey?username=bintray | sudo apt-key add -
```
## Red Hat/CentOS
```
wget https://bintray.com/loadimpact/rpm/rpm -O bintray-loadimpact-rpm.repo
sudo mv bintray-loadimpact-rpm.repo /etc/yum.repos.d/
sudo yum install k6
```
## Mac (brew)

## Brew
```
brew install k6
```
## Docker
```
docker pull loadimpact/k6
```

# Usage 

```
k6 run magento.js -e url=https://example.com -u 200 -i 6000 --include-system-env-vars=false
```
where:

magento.js - script name  
-e url= site url to test as env varriable  
-e sleep=0.2 etc. add sleep after http response
-u virtual users / concurancy / threads  
-i number of iteration  
--include-system-env-vars=false - don't include OS/system env varriables  

# Full K6 documentation

[https://k6.io/docs/](https://k6.io/docs/)


